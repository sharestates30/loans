﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Core;
using Api.Services;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace Api.Extensions
{
    public static class FilterSecurityExtension
    {
        public static List<string> AllowFilterStatus(this ClaimsPrincipal user, string stageCode, string stageGroupCode, LoanDbContext dbContext)
        {
            var roles = user.Claims.Where(c => c.Type.Equals(ClaimTypes.Role)).Select(c => c.Value).Distinct().ToList();

            var status = (from sg in dbContext.StageGroups
                          join s in dbContext.Stages on sg.StageGroupId equals s.StageGroupId
                          join sr in dbContext.StageRoles on s.StageId equals sr.StageId
                          join ss in dbContext.StageStatus on s.StageId equals ss.StageId
                          //where roles.Contains(sr.RoleId) && s.StageCode.Equals(stageCode, StringComparison.OrdinalIgnoreCase) && sg.StageGroupCode.Equals(stageGroupCode, StringComparison.OrdinalIgnoreCase)
                          where s.StageCode.Equals(stageCode, StringComparison.OrdinalIgnoreCase) && sg.StageGroupCode.Equals(stageGroupCode, StringComparison.OrdinalIgnoreCase)
                          select new
                          {
                              status = ss.ProcessStatus
                          }).ToList();

            return status.Select(c => c.status).Distinct().ToList();
        }

        public static string GetAssignerOwner(this ClaimsPrincipal user)
        {
            var sub = user.Claims.FirstOrDefault(c => c.Type == "sub");
            var roles = user.Claims.Where(c => c.Type.Equals(ClaimTypes.Role)).Select(c => c.Value);

            if (roles.Contains(Constants.RolesNames.Opener) || roles.Contains(Constants.RolesNames.LeadOpener))
            {
                return string.Empty;
            }

            if (roles.Contains(Constants.RolesNames.SubOpener))
            {
                return sub.Value;
            }

            if (roles.Contains(Constants.RolesNames.Underwriter) || roles.Contains(Constants.RolesNames.LeadUnderwriter) || roles.Contains(Constants.RolesNames.LeadOriginator))
            {
                return string.Empty;
            }

            if (roles.Contains(Constants.RolesNames.SubUnderwriter) || roles.Contains(Constants.RolesNames.SubOriginator))
            {
                return sub.Value;
            }

            if (roles.Contains(Constants.RolesNames.Processor) || roles.Contains(Constants.RolesNames.LeadProcessor))
            {
                return string.Empty;
            }

            if (roles.Contains(Constants.RolesNames.SubProcessor))
            {
                return sub.Value;
            }

            return string.Empty;
        }

        public static bool IsLeadUser(this ClaimsPrincipal user)
        {
            var sub = user.Claims.FirstOrDefault(c => c.Type == "sub");
            var roles = user.Claims.Where(c => c.Type.Equals(ClaimTypes.Role)).Select(c => c.Value);

            if (roles.Contains(Constants.RolesNames.LeadOpener))
            {
                return true;
            }

            if (roles.Contains(Constants.RolesNames.LeadClosingDesk))
            {
                return true;
            }

            if (roles.Contains(Constants.RolesNames.LeadPostCloser))
            {
                return true;
            }

            if (roles.Contains(Constants.RolesNames.LeadUnderwriter))
            {
                return true;
            }

            if (roles.Contains(Constants.RolesNames.LeadProcessor))
            {
                return true;
            }

            return false;
        }
    }
}
