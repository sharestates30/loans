﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;
using Microsoft.AspNetCore.Authorization;
using Api.Extensions;

namespace Api.Controllers
{
    public partial class LoanController
    {

        [HttpPut("{id}/reject")]
        public async Task<IActionResult> ChangeToReject(Guid id, [FromBody] LoanChangeToRejectPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();


                var content = $"Changed status to Rejected";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.Rejected, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.Rejected));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);
                entity.ReasonRejected = model.Comment;
                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanRejectQueue, new
                {
                    LoanId = entity.LoanId,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    IsBorrower = entity.BrokerId.HasValue ? false : true,
                });

                return this.NoContent();
            }
        }

        [HttpPut("{id}/pendingToAuthorize")]
        public async Task<IActionResult> ChangeToPendingToAuthorize(Guid id, [FromBody] LoanChangeToPendingToAuthorizePostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                
                var content = $"Changed status to Pending To Authorize";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.PendingToAuthorize, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.PendingToAuthorize));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);
                entity.SubmittedDay = DateTime.UtcNow;

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanPendingToAuthorizeQueue, new
                {
                    LoanId = entity.LoanId,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    IsBorrower = entity.BrokerId.HasValue ? false : true,
                });

                return this.NoContent();
            }
        }
        
        [HttpPut("{id}/pending")]
        public async Task<IActionResult> ChangeToPending(Guid id, [FromBody] LoanChangeToPendingPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Pending";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.PendingToReview, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.PendingToReview));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);
                entity.SubmittedDay = DateTime.UtcNow;

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanPendingQueue, new
                {
                    LoanId = entity.LoanId,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    IsBorrower = entity.BrokerId.HasValue ? false : true,
                });

                return this.NoContent();
            }
        }


        [HttpPut("{id}/uploadLoanForm")]
        public async Task<IActionResult> UploadLoanApplicationForm(Guid id, [FromBody] LoanUploadLoanFormPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Documents).ThenInclude(x => x.Files)
                            .Include(c => c.Documents).ThenInclude(x => x.Document)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();


                foreach (var item in entity.Documents.Where(c => c.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.LoanApplicationForm)))
                {
                    item.Status = DocumentStatusTypeEnum.Sent;
                    item.UpdatedBy = this.SubjectId;
                    item.UpdatedByName = this.SubjectName;
                    item.UpdatedOn = DateTime.UtcNow;

                    item.AddFile(model.LoanApplicationFormFileId, model.LoanApplicationFormFileName, model.LoanApplicationFormFileLink, this.SubjectId, this.SubjectName);
                }

                context.Loans.Update(entity);
                await context.SaveChangesAsync();
                
                return this.NoContent();
            }
        }

        [HttpPut("{id}/accepted")]
        public async Task<IActionResult> ChangeToAccepted(Guid id, [FromBody] LoanChangeToAcceptedPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Accepted";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var isLeader = this.User.IsLeadUser();
                
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var statusKey = string.Empty;

                if (isLeader)
                {
                    statusKey = Constants.LoanStatus.Accepted;
                }
                else {
                    statusKey = Constants.LoanStatus.PendingAccepted;
                }

                entity.AddLog(statusKey, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);

                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(statusKey));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);
                entity.IsPipeline = isLeader ? true : false;
                entity.IsApproved = isLeader ? true : false;

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                if (isLeader) {
                    await this._queueService.SendMessage(Constants.QueueNames.LoanAcceptedQueue, new
                    {
                        LoanId = entity.LoanId,
                        DeveloperId = entity.BorrowerId,
                        DeveloperName = entity.BrokerEntityName,
                        BrokerId = entity.BrokerId,
                        BrokerName = entity.BrokerEntityName,
                        IsBorrower = entity.BrokerId.HasValue ? false : true,
                    });
                }

                return this.NoContent();
            }
        }

        [HttpPut("{id}/termSheetUnderReview")]
        public async Task<IActionResult> ChangeToTermSheetUnderReview(Guid id, [FromBody] LoanChangeToTermSheetUnderReviewPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Term Sheet Under Review";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.TermSheetUnderReview, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.TermSheetUnderReview));
                entity.TermSheetChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                //await this._queueService.SendMessage(Constants.QueueNames.LoanUnderReviewTermSheetQueue, new
                //{
                //    LoanId = entity.LoanId,
                //    DeveloperId = entity.BorrowerId,
                //    DeveloperName = entity.BrokerEntityName,
                //    BrokerId = entity.BrokerId,
                //    BrokerName = entity.BrokerEntityName,
                //    IsBorrower = entity.BrokerId.HasValue ? false : true,
                //});

                return this.NoContent();
            }
        }

        [HttpPut("{id}/termSheetReviewed")]
        public async Task<IActionResult> ChangeToTermSheetReviewed(Guid id, [FromBody] LoanChangeToTermSheetReviewedPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Term Sheet Reviewed";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var isLeader = this.User.IsLeadUser();

                var statusKey = string.Empty;

                if (isLeader)
                {
                    statusKey = Constants.LoanStatus.TermSheetReviewed;
                }
                else
                {
                    statusKey = Constants.LoanStatus.PendingTermSheetReviewed;
                }


                entity.AddLog(statusKey, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(statusKey));
                entity.TermSheetChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                if (isLeader) {
                    await this._queueService.SendMessage(Constants.QueueNames.LoanUnderReviewTermSheetQueue, new
                    {
                        LoanId = entity.LoanId,
                        DeveloperId = entity.BorrowerId,
                        DeveloperName = entity.BrokerEntityName,
                        BrokerId = entity.BrokerId,
                        BrokerName = entity.BrokerEntityName,
                        IsBorrower = entity.BrokerId.HasValue ? false : true,
                    });
                }

                return this.NoContent();
            }
        }

        [HttpPut("{id}/termSheetPendingSignature")]
        public async Task<IActionResult> ChangeToTermSheetPendingSignature(Guid id, [FromBody] LoanChangeToTermSheetPendingSignaturePostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .Include(c=> c.Documents).ThenInclude(x => x.Files)
                            .Include(c => c.Documents).ThenInclude(x => x.Document)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Pending Termsheet Signature";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                foreach (var item in entity.Documents.Where(c => c.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.TermSheetForm)))
                {
                    item.Status = DocumentStatusTypeEnum.Sent;
                    item.UpdatedBy = this.SubjectId;
                    item.UpdatedByName = this.SubjectName;
                    item.UpdatedOn = DateTime.UtcNow;

                    item.AddFile(model.TermSheetFileId, model.TermSheetFileName, model.TermSheetFileLink, this.SubjectId, this.SubjectName);
                } 

                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.PendingTermsheetSignature, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.PendingTermsheetSignature));
                entity.TermSheetChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                return this.NoContent();
            }
        }

        [HttpPut("{id}/termSheetSigned")]
        public async Task<IActionResult> ChangeToSignedTermSheet(Guid id, [FromBody] LoanChangeToSignedTermSheetPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                             .Include(c => c.Documents).ThenInclude(x => x.Files)
                            .Include(c => c.Documents).ThenInclude(x => x.Document)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Term Sheet Signed";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                foreach (var item in entity.Documents.Where(c => c.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.TermSheetSignedForm)))
                {
                    item.Status = DocumentStatusTypeEnum.Sent;
                    item.UpdatedBy = this.SubjectId;
                    item.UpdatedByName = this.SubjectName;
                    item.UpdatedOn = DateTime.UtcNow;

                    item.AddFile(model.TermSheetSignedFileId, model.TermSheetSignedFileName, model.TermSheetSignedFileLink, this.SubjectId, this.SubjectName);
                }

                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.SignedTermSheet, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.SignedTermSheet));
                entity.TermSheetChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                return this.NoContent();
            }
        }

        [HttpPut("{id}/termSheetExpired")]
        public async Task<IActionResult> ChangeToExpiredTermSheet(Guid id, [FromBody] LoanChangeToExpiredTermSheetPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Term Sheet Expired";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.ExpiredTermSheet, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.ExpiredTermSheet));
                entity.TermSheetChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanExpiredTermSheetQueue, new
                {
                    LoanId = entity.LoanId,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    IsBorrower = entity.BrokerId.HasValue ? false : true,
                });

                return this.NoContent();
            }
        }

        [HttpPut("{id}/acceptedWithDeposit")]
        public async Task<IActionResult> ChangeToAcceptedWithDeposit(Guid id, [FromBody] LoanChangeToAcceptedDepositPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                             .Include(c => c.Documents).ThenInclude(x => x.Files)
                            .Include(c => c.Documents).ThenInclude(x => x.Document)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Accepted with Deposit";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                if (!model.Approve) {
                    var documentCodes = new List<string>() { Constants.LoanDocumentCodes.CreditAuthorizationForm, Constants.LoanDocumentCodes.CreditCardAuthorizationForm, Constants.LoanDocumentCodes.CreditCardDepositReceipt };
                    foreach (var item in entity.Documents.Where(c => documentCodes.Contains(c.Document.DocumentCode)))
                    {
                        item.Status = DocumentStatusTypeEnum.Sent;
                        item.UpdatedBy = this.SubjectId;
                        item.UpdatedByName = this.SubjectName;
                        item.UpdatedOn = DateTime.UtcNow;

                        if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.CreditAuthorizationForm, StringComparison.OrdinalIgnoreCase)) 
                            item.AddFile(model.CreditAuthorizationFormFileId, model.CreditAuthorizationFormFileName, model.CreditAuthorizationFormFileLink, this.SubjectId, this.SubjectName);
                        else if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.CreditCardAuthorizationForm, StringComparison.OrdinalIgnoreCase)) 
                            item.AddFile(model.CreditCardAuthorizationFormFileId, model.CreditCardAuthorizationFormFileName, model.CreditCardAuthorizationFormFileLink, this.SubjectId, this.SubjectName);
                        else if(item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.CreditCardDepositReceipt, StringComparison.OrdinalIgnoreCase))  
                            item.AddFile(model.CreditCardDepositReceiptFileId, model.CreditCardDepositReceiptFileName, model.CreditCardDepositReceiptFileLink, this.SubjectId, this.SubjectName);
                    }
                }

                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var isLeader = this.User.IsLeadUser();

                var statusKey = string.Empty;

                if (isLeader)
                {
                    statusKey = Constants.LoanStatus.AcceptedWithDeposit;
                }
                else
                {
                    statusKey = Constants.LoanStatus.PendingAcceptedWithDeposit;
                }

                entity.AddLog(statusKey, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(statusKey));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                if (isLeader) {
                    await this._queueService.SendMessage(Constants.QueueNames.LoanAcceptedWithDepositQueue, new
                    {
                        LoanId = entity.LoanId,
                        DeveloperId = entity.BorrowerId,
                        DeveloperName = entity.BrokerEntityName,
                        BrokerId = entity.BrokerId,
                        BrokerName = entity.BrokerEntityName,
                        IsBorrower = entity.BrokerId.HasValue ? false : true,
                    });
                }

                return this.NoContent();
            }
        }

        [HttpPut("{id}/acceptedWithDeposit/approve")]
        public async Task<IActionResult> ChangeToAcceptedWithDepositApprove(Guid id, [FromBody] LoanChangeToAcceptedDepositPostViewModel model)
        {
            model.Approve = true;
            return await this.ChangeToAcceptedWithDeposit(id, model);
        }


        [HttpPut("{id}/appraisalReceived")]
        public async Task<IActionResult> ChangeToAppraisalReceived(Guid id, [FromBody] LoanChangeToAppraisalReceivedPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Appraisal Received";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                if (!model.Approve)
                {
                    entity.AppraisalCompanyName = model.AppraisalCompanyName;
                    entity.AppraiserFee = model.AppraiserFee;
                    entity.AppraisalDateOrdered = model.AppraisalDateOrdered;
                    entity.AppraisalDateNeeded = model.AppraisalDateNeeded;
                    entity.AppraisalStatus = model.AppraisalStatus;
                }

                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var isLeader = this.User.IsLeadUser();

                var statusKey = string.Empty;

                if (isLeader)
                {
                    statusKey = Constants.LoanStatus.AppraisalReceived;
                }
                else
                {
                    statusKey = Constants.LoanStatus.PendingAppraisalReceived;
                }

                entity.AddLog(statusKey, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(statusKey));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                if (isLeader)
                {
                    await this._queueService.SendMessage(Constants.QueueNames.LoanAppraisalReceivedQueue, new
                    {
                        LoanId = entity.LoanId,
                        DeveloperId = entity.BorrowerId,
                        DeveloperName = entity.BrokerEntityName,
                        BrokerId = entity.BrokerId,
                        BrokerName = entity.BrokerEntityName,
                        IsBorrower = entity.BrokerId.HasValue ? false : true,
                    });
                }

                return this.NoContent();
            }
        }

        [HttpPut("{id}/appraisalReceived/approve")]
        public async Task<IActionResult> ChangeToAppraisalReceivedApprove(Guid id, [FromBody] LoanChangeToAppraisalReceivedPostViewModel model)
        {
            model.Approve = true;
            return await this.ChangeToAppraisalReceived(id, model);
        }


        [HttpPut("{id}/appraisalOrdered")]
        public async Task<IActionResult> ChangeToAppraisalOrdered(Guid id, [FromBody] LoanChangeToAppraisalOrderedPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                             .Include(c => c.Documents).ThenInclude(x => x.Files)
                            .Include(c => c.Documents).ThenInclude(x => x.Document)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Appraisal Ordered";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                if (!model.Approve) {
                    var documentCodes = new List<string>() { Constants.LoanDocumentCodes.Appraisal, Constants.LoanDocumentCodes.AppraisalInvoice };
                    foreach (var item in entity.Documents.Where(c => documentCodes.Contains(c.Document.DocumentCode)))
                    {
                        item.Status = DocumentStatusTypeEnum.Sent;
                        item.UpdatedBy = this.SubjectId;
                        item.UpdatedByName = this.SubjectName;
                        item.UpdatedOn = DateTime.UtcNow;

                        if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.Appraisal, StringComparison.OrdinalIgnoreCase))
                            item.AddFile(model.AppraisalFileId, model.AppraisalFileName, model.AppraisalFileLink, this.SubjectId, this.SubjectName);
                        else if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.AppraisalInvoice, StringComparison.OrdinalIgnoreCase))
                            item.AddFile(model.AppraisalInvoiceFileId, model.AppraisalInvoiceFileName, model.AppraisalInvoiceFileLink, this.SubjectId, this.SubjectName);
                    }
                }
                
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var isLeader = this.User.IsLeadUser();

                var statusKey = string.Empty;

                if (isLeader)
                {
                    statusKey = Constants.LoanStatus.AppraisalOrdered;
                }
                else
                {
                    statusKey = Constants.LoanStatus.PendingAppraisalOrdered;
                }

                entity.AddLog(statusKey, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(statusKey));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                if (isLeader) {
                    await this._queueService.SendMessage(Constants.QueueNames.LoanAppraisalOrderedQueue, new
                    {
                        LoanId = entity.LoanId,
                        DeveloperId = entity.BorrowerId,
                        DeveloperName = entity.BrokerEntityName,
                        BrokerId = entity.BrokerId,
                        BrokerName = entity.BrokerEntityName,
                        IsBorrower = entity.BrokerId.HasValue ? false : true,
                    });
                }

                return this.NoContent();
            }
        }

        [HttpPut("{id}/appraisalOrdered/approve")]
        public async Task<IActionResult> ChangeToAppraisalOrderedApprove(Guid id, [FromBody] LoanChangeToAppraisalOrderedPostViewModel model)
        {
            model.Approve = true;
            return await this.ChangeToAppraisalOrdered(id, model);
        }

        [HttpPut("{id}/appraisalReportSent")]
        public async Task<IActionResult> ChangeToAppraisalReportSent(Guid id, [FromBody] LoanChangeToAppraisalReportSentPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Appraisal Report Sent";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var isLeader = this.User.IsLeadUser();

                var statusKey = string.Empty;

                if (isLeader)
                {
                    statusKey = Constants.LoanStatus.AppraisalReportSent;
                }
                else
                {
                    statusKey = Constants.LoanStatus.PendingAppraisalReportSent;
                }

                entity.AddLog(statusKey, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(statusKey));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                if (isLeader) {
                    await this._queueService.SendMessage(Constants.QueueNames.LoanAppraisalReportSentQueue, new
                    {
                        LoanId = entity.LoanId,
                        DeveloperId = entity.BorrowerId,
                        DeveloperName = entity.BrokerEntityName,
                        BrokerId = entity.BrokerId,
                        BrokerName = entity.BrokerEntityName,
                        IsBorrower = entity.BrokerId.HasValue ? false : true,
                    });
                }

                return this.NoContent();
            }
        }

        [HttpPut("{id}/appraisalReportSent/approve")]
        public async Task<IActionResult> ChangeToAppraisalReportSentApprove(Guid id, [FromBody] LoanChangeToAppraisalReportSentPostViewModel model)
        {
            model.Approve = true;
            return await this.ChangeToAppraisalReportSent(id, model);
        }

        [HttpPut("{id}/appraisalApproved")]
        public async Task<IActionResult> ChangeToAppraisalApproved(Guid id, [FromBody] LoanChangeToAppraisalReceivedPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Appraisal Approved";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var isLeader = this.User.IsLeadUser();

                var statusKey = string.Empty;

                if (isLeader)
                {
                    statusKey = Constants.LoanStatus.AppraisalApproved;
                }
                else
                {
                    statusKey = Constants.LoanStatus.PendingAppraisalApproved;
                }

                entity.AddLog(statusKey, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(statusKey));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                if (isLeader)
                {
                    await this._queueService.SendMessage(Constants.QueueNames.LoanAppraisalApprovedQueue, new
                    {
                        LoanId = entity.LoanId,
                        DeveloperId = entity.BorrowerId,
                        DeveloperName = entity.BrokerEntityName,
                        BrokerId = entity.BrokerId,
                        BrokerName = entity.BrokerEntityName,
                        IsBorrower = entity.BrokerId.HasValue ? false : true,
                    });
                }

                return this.NoContent();
            }
        }

        [HttpPut("{id}/appraisalApproved/approve")]
        public async Task<IActionResult> ChangeToAppraisalApprovedApprove(Guid id, [FromBody] LoanChangeToAppraisalReceivedPostViewModel model)
        {
            model.Approve = true;
            return await this.ChangeToAppraisalApproved(id, model);
        }

        [HttpPut("{id}/titleRequired")]
        public async Task<IActionResult> ChangeToTitleRequired(Guid id, [FromBody] LoanChangeToTitleRequiredPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Title Required";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.TitleRequired, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.TitleRequired));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanTitleRequiredQueue, new
                {
                    LoanId = entity.LoanId,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    IsBorrower = entity.BrokerId.HasValue ? false : true,
                });

                return this.NoContent();
            }
        }

        [HttpPut("{id}/UWClear")]
        public async Task<IActionResult> ChangeToUWClear(Guid id, [FromBody] LoanChangeToUWClearPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to UW Clear";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                entity.TitleCompanyId = model.TitleCompanyId;
                entity.TitleBusinessName = model.TitlePropertyBusinessName;
                entity.TitleCompanyFirstName = model.TitlePropertyFirstName;
                entity.TitleCompanyLastName = model.TitlePropertyLastName;
                entity.TitlePropertyNumber = model.TitlePropertyNumber;
                entity.TitleReportBackDate = model.TitleReportBackDate;
                entity.TitleReportOrderedDate = model.TitleReportOrderedDate;
                entity.TitleReportStatus = model.TitleReportStatus;
                
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.UWClear, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.UWClear));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanUWClearQueue, new
                {
                    LoanId = entity.LoanId,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    IsBorrower = entity.BrokerId.HasValue ? false : true,
                });

                return this.NoContent();
            }
        }

        [HttpPut("{id}/titleReceived")]
        public async Task<IActionResult> ChangeToTitleReceived(Guid id, [FromBody] LoanChangeToTitleReceivePostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                             .Include(c => c.Documents).ThenInclude(x => x.Files)
                            .Include(c => c.Documents).ThenInclude(x => x.Document)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Title Received";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var documentCodes = new List<string>() { Constants.LoanDocumentCodes.TitleApplicationForm, Constants.LoanDocumentCodes.TitleReport, Constants.LoanDocumentCodes.TitlePolicy };
                foreach (var item in entity.Documents.Where(c => documentCodes.Contains(c.Document.DocumentCode)))
                {
                    item.Status = DocumentStatusTypeEnum.Sent;
                    item.UpdatedBy = this.SubjectId;
                    item.UpdatedByName = this.SubjectName;
                    item.UpdatedOn = DateTime.UtcNow;

                    if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.TitleApplicationForm, StringComparison.OrdinalIgnoreCase))
                        item.AddFile(model.TitleApplicationFormFileId, model.TitleApplicationFormFileName, model.TitleApplicationFormFileLink, this.SubjectId, this.SubjectName);
                    else if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.TitleReport, StringComparison.OrdinalIgnoreCase))
                        item.AddFile(model.TitleReportFileId, model.TitleReportFileName, model.TitleReportFileLink, this.SubjectId, this.SubjectName);
                    else if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.TitlePolicy, StringComparison.OrdinalIgnoreCase))
                        item.AddFile(model.TitlePolicyFileId, model.TitlePolicyFileName, model.TitlePolicyFileLink, this.SubjectId, this.SubjectName);
                }
                
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.TitleReceived, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.TitleReceived));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanTitleReceivedQueue, new
                {
                    LoanId = entity.LoanId,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    IsBorrower = entity.BrokerId.HasValue ? false : true,
                });

                return this.NoContent();
            }
        }

        [HttpPut("{id}/UWTitleClear")]
        public async Task<IActionResult> ChangeToUWClearTitle(Guid id, [FromBody] LoanChangeToUWTitleClearPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to UW Title Clear";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                entity.TitlePropertyNumber = model.TitlePropertyNumber;
                
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.UWTitleClear, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.UWTitleClear));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanUWTitleClearQueue, new
                {
                    LoanId = entity.LoanId,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    IsBorrower = entity.BrokerId.HasValue ? false : true,
                });

                return this.NoContent();
            }
        }

        [HttpPut("{id}/lenderInstructionsIssued")]
        public async Task<IActionResult> ChangeToLenderInstructionsIssued(Guid id, [FromBody] LoanChangeToLenderInstructionsIssuedPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                             .Include(c => c.Documents).ThenInclude(x => x.Files)
                            .Include(c => c.Documents).ThenInclude(x => x.Document)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var content = $"Changed status to Lender Instructions Issued";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var documentCodes = new List<string>() { Constants.LoanDocumentCodes.LenderLetter };
                foreach (var item in entity.Documents.Where(c => documentCodes.Contains(c.Document.DocumentCode)))
                {
                    item.Status = DocumentStatusTypeEnum.Sent;
                    item.UpdatedBy = this.SubjectId;
                    item.UpdatedByName = this.SubjectName;
                    item.UpdatedOn = DateTime.UtcNow;

                    item.AddFile(model.LenderLetterFileId, model.LenderLetterFileName, model.LenderLetterFileLink, this.SubjectId, this.SubjectName);
                }
                
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.LenderInstructionsIssued, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.LenderInstructionsIssued));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanLenderInstructionsIssuedQueue, new
                {
                    LoanId = entity.LoanId,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    IsBorrower = entity.BrokerId.HasValue ? false : true,
                });

                return this.NoContent();
            }
        }

        [HttpPut("{id}/closed")]
        public async Task<IActionResult> ChangeToClosed(Guid id, [FromBody] LoanChangeToClosedPostViewModel model)
        {

            using (var context = this._dbContext)
            {
                var entity = context
                            .Loans
                            .Include(c => c.Logs)
                            .Include(c => c.Trackings)
                             .Include(c => c.Documents).ThenInclude(x => x.Files)
                            .Include(c => c.Documents).ThenInclude(x => x.Document)
                            .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();


	            entity.OriginationEntity = model.OriginationEntity ;
				entity.TargetClosingDate = model.TargetClosingDate;
				entity.ClosingDate = model.ClosingDate;
				entity.Location = model.Location;
				entity.ClosingTime = model.ClosingTime;
				entity.DisbursedAmountatClosing = model.DisbursedAmountatClosing;
				entity.EscrowHoldBack = model.EscrowHoldBack;

                var content = $"Changed status to Closed";
                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                var documentCodes = new List<string>() {
                    Constants.LoanDocumentCodes.RecordedMortgage,
                    Constants.LoanDocumentCodes.RecordedUCCs,
                    Constants.LoanDocumentCodes.FinalLoanPolicy,
                    Constants.LoanDocumentCodes.ClosingPackage,
                    Constants.LoanDocumentCodes.ForbearanceAgreement,
                    Constants.LoanDocumentCodes.PropertyTaxes
                };

                foreach (var item in entity.Documents.Where(c => documentCodes.Contains(c.Document.DocumentCode)))
                {
                    item.Status = DocumentStatusTypeEnum.Sent;
                    item.UpdatedBy = this.SubjectId;
                    item.UpdatedByName = this.SubjectName;
                    item.UpdatedOn = DateTime.UtcNow;

                    if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.RecordedMortgage, StringComparison.OrdinalIgnoreCase))
                        item.AddFile(model.RecordedMortgageFileId, model.RecordedMortgageFileName, model.RecordedMortgageFileLink, this.SubjectId, this.SubjectName);
                    else if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.RecordedUCCs, StringComparison.OrdinalIgnoreCase))
                        item.AddFile(model.RecordedUCCsFileId, model.RecordedUCCsFileName, model.RecordedUCCsFileLink, this.SubjectId, this.SubjectName);
                    else if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.FinalLoanPolicy, StringComparison.OrdinalIgnoreCase))
                        item.AddFile(model.FinalLoanPolicyFileId, model.FinalLoanPolicyFileName, model.FinalLoanPolicyFileLink, this.SubjectId, this.SubjectName);
                    else if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.ClosingPackage, StringComparison.OrdinalIgnoreCase))
                        item.AddFile(model.ClosingPackageFileId, model.ClosingPackageFileName, model.ClosingPackageFileLink, this.SubjectId, this.SubjectName);
                    else if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.ForbearanceAgreement, StringComparison.OrdinalIgnoreCase))
                        item.AddFile(model.ForbearanceAgreementFileId, model.ForbearanceAgreementFileName, model.ForbearanceAgreementFileLink, this.SubjectId, this.SubjectName);
                    else if (item.Document.DocumentCode.Equals(Constants.LoanDocumentCodes.PropertyTaxes, StringComparison.OrdinalIgnoreCase))
                        item.AddFile(model.PropertyTaxesFileId, model.PropertyTaxesFileName, model.PropertyTaxesFileLink, this.SubjectId, this.SubjectName);
                }
                
                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.LoanStatus.Closed, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                var statusList = await this._loanStatus.GetStatus();
                var status = statusList.FirstOrDefault(c => c.Value.Equals(Constants.LoanStatus.Closed));
                entity.ChangeStatus(status.Value, status.Name, this.SubjectId, this.SubjectName, content);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanClosedQueue, new
                {
                    LoanId = entity.LoanId,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    IsBorrower = entity.BrokerId.HasValue ? false : true,
                });

                return this.NoContent();
            }
        }

    }
}

