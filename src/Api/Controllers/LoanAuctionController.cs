﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;
using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
    public partial class LoanController
    {
        [HttpPost("{id}/bid")]
        public async Task<IActionResult> Bid(Guid id, [FromBody]LoanBidPostViewModel model)
        {
            if (!ModelState.IsValid)
                return this.BadRequest(ModelState);

            using (var context = this._dbContext)
            {
                var entity = await context.Loans
                        .Include(c=> c.Auctions)
                        .FirstOrDefaultAsync(c => c.LoanId.Equals(id));

                entity.AddBid(model.VendorId, model.VendorName, model.Bid, (AuctionTypeEnum)model.AuctionType, model.Comment, this.SubjectId, this.SubjectName);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                return this.Ok();
            }
        }
        

        [HttpGet("{id}/bid")]
        [AllowAnonymous]
        public async Task<IActionResult> GetBids(Guid id, 
            [FromQuery(Name = "type")]int type, 
            [FromQuery(Name = "vendorId")]Guid? vendorId)
        {
            if (!ModelState.IsValid)
                return this.BadRequest(ModelState);

            using (var context = this._dbContext)
            {
                var model = (from a in context.Auctions
                             where (int)a.LoanAuctionType == type
                             orderby a.CreatedOn descending
                             select new LoanBidViewModel
                             {
                                 LoanAuctionId = a.LoanAuctionId,
                                 LoanId = a.LoanId,
                                 Awarded = a.Awarded,
                                 Bid = a.Bid,
                                 Content = a.Content,
                                 LoanAuctionType = (int)a.LoanAuctionType,
                                 LoanAuctionTypeName = a.LoanAuctionType.ToString(),
                                 VendorId = a.VendorId,
                                 VendorName = a.VendorName,
                                 CreatedOn = a.CreatedOn
                             });

                if (vendorId.HasValue)
                    model = model.Where(c => c.VendorId.Equals(vendorId));

                var result = await model.ToListAsync();

                return this.Ok(result);
            }
        }

        [HttpPost("{id}/bid/{bidId}/{type}/awarded")]
        public async Task<IActionResult> Awarded(Guid id, Guid bidId, int type, [FromBody]LoanAwardedPostViewModel model)
        {
            if (!ModelState.IsValid)
                return this.BadRequest(ModelState);

            using (var context = this._dbContext)
            {
                var entity = await context.Loans
                        .Include(c => c.Auctions)
                        .FirstOrDefaultAsync(c => c.LoanId.Equals(id));

                entity.Awarded(bidId, (AuctionTypeEnum)type);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                return this.Ok();
            }
        }
        
    }
}

