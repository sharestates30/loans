﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanViewModel
    {
        public Guid LoanId { get; set; }
        public string LoanNumber { get; set; }
        public bool IsPipeline { get; set; }
        public bool IsApproved { get; set; }

        public int LoanPurposeType { get; set; }
        public string LoanPurposeTypeName { get; set; }
        public int TransactionType { get; set; }
        public string TransactionTypeName { get; set; }
        public bool HasSalesRepresentative { get; set; }
        public string SalesPersonId { get; set; }
        public string SalesPersonName { get; set; }

        public bool HasRehabComponent { get; set; }

        public decimal PurchasePrice { get; set; }
        public decimal ConstructionBudget { get; set; }

        public decimal AsIsValue { get; set; }
        public decimal FinalValue { get; set; }
        public decimal AquisitionLoanRequest { get; set; }
        public decimal ConstructionLoanRequest { get; set; }

        public DateTime? OriginalPurchaseDate { get; set; }
        public decimal? OriginalPurchasePrice { get; set; }

        /// <summary>
        /// 12 months
        /// 24 months
        /// 36 months
        /// </summary>
        public int LoanTermRequest { get; set; }
        public bool NeedMoreTerm { get; set; }
        public int? MoreTermValue { get; set; }

        public DateTime DesiredFundingDate { get; set; }
        public bool IsTOETransaction { get; set; }
        public DateTime? TOEClosingDate { get; set; }

        public bool IsShortSaleTransactionType { get; set; }
        public bool HasAssignmentFlipType { get; set; }
        public decimal? ContractorAssignedValue { get; set; }
        public string AssignorName { get; set; }
        public bool HasAssignorAffiliate { get; set; }
        public string AssignorAffiliateExplanation { get; set; }

        public bool IsLLCTransactionType { get; set; }

        public Guid? EntityId { get; set; }
        public string EntityName { get; set; }

        public string Address { get; set; }
        public string Address2 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public int CurrentAssetType { get; set; }
        public int CurrentAssetTypeValue { get; set; }

        public int Occupancy { get; set; }

        public int NewAssetType { get; set; }
        public int NewAssetTypeValue { get; set; }

        public int DevelopmentPhase { get; set; }
        public int ExitStrategy { get; set; }

        public Guid? BrokerId { get; set; }
        public string BrokerAgentName { get; set; }
        public string BrokerEntityName { get; set; }

        public Guid? BorrowerId { get; set; }
        public string BorrowerName { get; set; }
        public string BorrowerEntityName { get; set; }

        /// <summary>
        /// Below 650
        /// 650-679
        /// 680-699
        /// 700-759
        /// 760-850
        /// </summary>
        public int BorrowerCreditScore { get; set; }

        public bool HasBankruptcyType { get; set; }
        public string BankruptcyExplanation { get; set; }
        public int BankruptcyYears { get; set; }

        public bool HasDefaultedLoanType { get; set; }
        public string DefaultedLoanExplanation { get; set; }

        public bool HasFiledForeclosure { get; set; }
        public string FiledForeclosureExplanation { get; set; }

        public bool HasOutstandingJudgmentsType { get; set; }
        public string OutstandingJudgmentsExplanation { get; set; }

        public Guid? AppraiserId { get; set; }
        public string AppraisalCompanyName { get; set; }
        public string AppraiserName { get; set; }
        public decimal? AppraiserFee { get; set; }
        public DateTime? AppraisalDateOrdered { get; set; }
        public DateTime? AppraisalDateNeeded { get; set; }

        /// <summary>
        /// Pending
        /// Received
        /// Ordered
        /// </summary>
        public string AppraisalStatus { get; set; }

        public Guid? TitleCompanyId { get; set; }
        public string TitlePropertyNumber { get; set; }
        public string TitleBusinessName { get; set; }
        public string TitleCompanyFirstName { get; set; }
        public string TitleCompanyLastName { get; set; }

        public string ProcessStatus { get; set; }
        public string ProcessStatusName { get; set; }
        public string TermSheetStatus { get; set; }
        public string TermSheetStatusName { get; set; }

        public DateTime? SubmittedDay { get; set; }

        public string ReasonRejected { get; set; }

        public string OriginationEntity { get; set; }
        public DateTime? TargetClosingDate { get; set; }
        public DateTime? ClosingDate { get; set; }
        public string Location { get; set; }
        public int TimeOfClosing { get; set; }
        public DateTime? ClosingTime { get; set; }
        public decimal DisbursedAmountatClosing { get; set; }
        public decimal EscrowHoldBack { get; set; }
    }
}
