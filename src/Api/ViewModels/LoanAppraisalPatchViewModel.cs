﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanAppraisalPatchViewModel
    {
        public string AppraisalCompanyName { get; set; }
        public string AppraiserName { get; set; }
        public string AppraiserAddress { get; set; }
        public string AppraiserCountry { get; set; }
        public string AppraiserCity { get; set; }
        public string AppraiserState { get; set; }
        public string AppraiserZipCode { get; set; }
        public string AppraiserEmail { get; set; }
        public string AppraiserPhone { get; set; }
    }
}
