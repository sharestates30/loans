﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToTermSheetUnderReviewPostViewModel
    {

    }

    public class LoanChangeToTermSheetReviewedPostViewModel
    {

    }

    public class LoanChangeToTermSheetPendingSignaturePostViewModel
    {
        /// <summary>
        /// Documents : Term Sheet
        /// </summary>
        public string TermSheetFileId { get; set; }
        public string TermSheetFileName { get; set; }
        public string TermSheetFileLink { get; set; }

    }
}
