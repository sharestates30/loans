﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanUploadLoanFormPostViewModel
    {

        /// <summary>
        /// Documents : Loan Form
        /// </summary>
        public string LoanApplicationFormFileId { get; set; }
        public string LoanApplicationFormFileName { get; set; }
        public string LoanApplicationFormFileLink { get; set; }

        public string Comment { get; set; }
    }
}
