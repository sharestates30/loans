﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanListViewModel
    {
        public Guid LoanId { get; set; }
        public string LoanNumber { get; set; }
        public bool IsPipeline { get; set; }
        public bool IsApproved { get; set; }

        public int LoanPurposeType { get; set; }
        public string LoanPurposeTypeName { get; set; }
        public int TransactionType { get; set; }
        public string TransactionTypeName { get; set; }
        public bool HasSalesRepresentative { get; set; }

        public bool HasRehabComponent { get; set; }

        public decimal PurchasePrice { get; set; }
        public decimal ConstructionBudget { get; set; }

        public decimal AsIsValue { get; set; }
        public decimal FinalValue { get; set; }
        public decimal AquisitionLoanRequest { get; set; }
        public decimal ConstructionLoanRequest { get; set; }

        public DateTime? OriginalPurchaseDate { get; set; }
        public decimal? OriginalPurchasePrice { get; set; }
        public int LoanTermRequest { get;  set; }
        public DateTime DesiredFundingDate { get;  set; }
        public string Address { get;  set; }
        public string Country { get;  set; }
        public string City { get;  set; }
        public string State { get;  set; }
        public string ZipCode { get;  set; }
        public string BorrowerName { get;  set; }
        public string BrokerEntityName { get;  set; }
        public int BorrowerCreditScore { get;  set; }
        public Guid? BorrowerId { get;  set; }
        public Guid? BrokerId { get;  set; }
        public DateTime? SubmittedDay { get;  set; }
        public string ProcessStatus { get;  set; }
        public string BrokerAgentName { get;  set; }
        public string BorrowerEntityName { get;  set; }
        public string TermSheetStatus { get;  set; }
        public DateTime CreatedOn { get;  set; }
        public string ProcessStatusName { get; set; }
        public string TermSheetStatusName { get;  set; }
    }
}
