﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanAssignToPostViewModel
    {
        public string OwnerUserId { get; set; }
        public string OwnerUserName { get; set; }
        public string Comment { get; set; }
    }

    public class LoanAttorneyUserToPostViewModel
    {
        public string AttorneyUserId { get; set; }
        public string AttorneyUserName { get; set; }
        public string Comment { get; set; }
    }
}
