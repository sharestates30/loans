﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToSignedTermSheetPostViewModel
    {
        /// <summary>
        /// Documents : Term Sheet Signed
        /// </summary>
        public string TermSheetSignedFileId { get; set; }
        public string TermSheetSignedFileName { get; set; }
        public string TermSheetSignedFileLink { get; set; }
    }
}
