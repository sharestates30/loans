﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToClosedPostViewModel
    {
        /// <summary>
        /// Documents : Recorded Mortgage
        /// </summary>
        public string RecordedMortgageFileId { get; set; }
        public string RecordedMortgageFileName { get; set; }
        public string RecordedMortgageFileLink { get; set; }

        /// <summary>
        /// Documents : Recorded UCCs
        /// </summary>
        public string RecordedUCCsFileId { get; set; }
        public string RecordedUCCsFileName { get; set; }
        public string RecordedUCCsFileLink { get; set; }

        /// <summary>
        /// Documents : Final Loan Policy
        /// </summary>
        public string FinalLoanPolicyFileId { get; set; }
        public string FinalLoanPolicyFileName { get; set; }
        public string FinalLoanPolicyFileLink { get; set; }

        /// <summary>
        /// Documents : Closing Package
        /// </summary>
        public string ClosingPackageFileId { get; set; }
        public string ClosingPackageFileName { get; set; }
        public string ClosingPackageFileLink { get; set; }

        /// <summary>
        /// Documents : Forbearance Agreement
        /// </summary>
        public string ForbearanceAgreementFileId { get; set; }
        public string ForbearanceAgreementFileName { get; set; }
        public string ForbearanceAgreementFileLink { get; set; }

        /// <summary>
        /// Documents : Property Taxes
        /// </summary>
        public string PropertyTaxesFileId { get; set; }
        public string PropertyTaxesFileName { get; set; }
        public string PropertyTaxesFileLink { get; set; }

		public string OriginationEntity { get; set; }
		public DateTime TargetClosingDate { get; set; }
		public DateTime ClosingDate { get; set; }
		public string Location { get; set; }
		public int TimeOfClosing { get; set; }
		public DateTime ClosingTime { get; set; }
		public decimal DisbursedAmountatClosing { get; set; }
		public decimal EscrowHoldBack { get; set; }

        public string Comment { get; set; }
    }
}
