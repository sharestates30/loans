﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanBidViewModel
    {
        public Guid LoanAuctionId { get; set; }

        public Guid VendorId { get; set; }
        public int LoanAuctionType { get; set; }
        public string LoanAuctionTypeName { get; set; }
        public string VendorName { get; set; }

        public decimal Bid { get; set; }
        public string Content { get; set; }

        public bool Awarded { get; set; }

        public Guid LoanId { get; set; }

        public DateTime CreatedOn { get; set; }
        
    }
}
