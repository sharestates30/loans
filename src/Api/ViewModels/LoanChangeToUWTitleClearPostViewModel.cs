﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToUWTitleClearPostViewModel
    {
        public string TitlePropertyNumber { get; set; }
        public string Comment { get; set; }
    }
}
