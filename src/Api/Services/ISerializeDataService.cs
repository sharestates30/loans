﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Services
{
    public interface ISerializeDataService
    {
        string SerializeLoan(Loan loan);
    }
}
