﻿using Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Api.Services
{
    public class CloudQueueServiceProvider
    {

        private static readonly string UserName = "chenton147";
        private static readonly string ApiKey = "465390f7e16e49a79f57a6b30c81e649";

        private readonly IHostingEnvironment _environment;
        public CloudQueueServiceProvider(IHostingEnvironment environment)
        {
            this._environment = environment;
        }

        private static readonly Lazy<CustomCloudIdentity> _customCloudIdentity = new Lazy<CustomCloudIdentity>(() => {

            var client = new HttpClient();

            var json = "{\"auth\":{\"RAX-KSKEY:apiKeyCredentials\":{\"username\":\"" + UserName + "\",\"apiKey\":\"" + ApiKey + "\"}}}";

            var request = new HttpRequestMessage(HttpMethod.Post, "https://identity.api.rackspacecloud.com/v2.0/tokens");
            request.Content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = client.SendAsync(request).Result;
            var customCloudIdentity = Newtonsoft.Json.JsonConvert.DeserializeObject<CustomCloudIdentity>(response.Content.ReadAsStringAsync().Result);

            return customCloudIdentity;

        });

        private CustomCloudIdentity CustomCloudIdentity { get { return _customCloudIdentity.Value; } }

        public CloudQueue GetQueue(string region, string queueName)
        {
            var endpointsQueue = CustomCloudIdentity.access.serviceCatalog.FirstOrDefault(c => c.name.Equals("cloudQueues"));
            var endpointRegionQueue = endpointsQueue.endpoints.FirstOrDefault(c => c.publicURL.IndexOf(region) > 0);

            var queue = new CloudQueue();

            queue.Url = endpointRegionQueue.publicURL;
            queue.Name = ResolvePrefix(queueName);
            queue.AccessToken = CustomCloudIdentity.access.token.id;
            queue.TenantId = CustomCloudIdentity.access.token.tenant.id;

            return queue;
        }

        public async Task SendQueue(CloudQueue queue, CloudMessageQueue message)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"{queue.Url}/queues/{queue.Name}/messages");

            var messages = new List<CloudMessageQueue>();
            messages.Add(message);

            request.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(messages), Encoding.UTF8, "application/json");
            request.Headers.TryAddWithoutValidation("Client-ID", $"{Guid.NewGuid()}");
            request.Headers.TryAddWithoutValidation("X-Auth-Token", queue.AccessToken);
            request.Headers.TryAddWithoutValidation("X-Project-Id", queue.TenantId);

            var client = new HttpClient();
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
                throw new Exception("Queue Error");
        }

        private string ResolvePrefix(string queueName)
        {
            if (_environment.EnvironmentName.Equals("Development", StringComparison.OrdinalIgnoreCase) || _environment.EnvironmentName.Equals("Local", StringComparison.OrdinalIgnoreCase))
                return $"dev-{queueName}";

            if (_environment.EnvironmentName.Equals("QA", StringComparison.OrdinalIgnoreCase))
                return $"qa-{queueName}";

            return queueName;
        }

    }

    public class CloudMessageQueue
    {

        public CloudMessageQueue(object body)
        {
            this.TTL = Convert.ToInt32(TimeSpan.FromMinutes(900).TotalSeconds);
            this.Body = body;
        }

        [JsonProperty("ttl")]
        public int TTL { get; set; }

        [JsonProperty("body")]
        public object Body { get; set; }
    }

    public class CloudQueue
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public string AccessToken { get; set; }
        public string TenantId { get; set; }
    }

    public class CustomCloudIdentity
    {
        public CloudAccess access { get; set; }
    }

    public class CloudEndpoint
    {
        public string tenantId { get; set; }
        public string publicURL { get; set; }
        public string region { get; set; }
        public string internalURL { get; set; }
        public string versionId { get; set; }
        public string versionList { get; set; }
        public string versionInfo { get; set; }
    }

    public class CloudServiceCatalog
    {
        public List<CloudEndpoint> endpoints { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }

    public class CloudRole
    {
        public string name { get; set; }
        public string description { get; set; }
        public string id { get; set; }
        public string tenantId { get; set; }
    }

    public class CloudUser
    {
        public List<CloudRole> roles { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }

    public class CloudTenant
    {
        public string name { get; set; }
        public string id { get; set; }
    }

    public class CloudToken
    {
        public string expires { get; set; }
        public string id { get; set; }
        public CloudTenant tenant { get; set; }
    }

    public class CloudAccess
    {
        public List<CloudServiceCatalog> serviceCatalog { get; set; }
        public CloudUser user { get; set; }
        public CloudToken token { get; set; }
    }

}
