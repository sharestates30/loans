﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Models
{
    public class LoanCreateNotificationMessage
    {
        public Guid LoanId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string DeveloperId { get; set; }
        public string DeveloperName { get; set; }
        public string BrokerId { get; set; }
        public string BrokerName { get; set; }
        public bool IsBorrower { get; set; }
        public string Content { get; set; }

    }
}
