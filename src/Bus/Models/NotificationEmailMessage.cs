﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Messages
{
    public class NotificationEmailMessage
    {
        public string NotificationTemplateCode { get; set; }
        public Dictionary<string, object> Model { get; set; }
        public Dictionary<string, string> Emails { get; set; }
    }
}
