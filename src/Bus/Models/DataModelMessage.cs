﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Models
{
    public class DataModelMessage
    {
        public string Key { get; set; }
        public object Value { get; set; }
    }
}
