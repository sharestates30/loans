﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Models
{
    public class LoanInfoViewModel
    {
        public LoanViewModel Loan { get; set; }
        public DeveloperViewModel Developer { get; set; }
        public BrokerViewModel Broker { get; set; }
    }
}
