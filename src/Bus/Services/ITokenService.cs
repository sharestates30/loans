﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Services
{
    public interface ITokenService
    {
        Task<OAuthToken> GetToken();
    }
}
