﻿using IdentityModel.Client;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Services
{
    public class TokenService : ITokenService
    {
        private static MemoryCache _cache = MemoryCache.Default;
        private readonly string key = "__token__";

        public async Task<OAuthToken> GetToken()
        {
            var token = _cache.Get(key) as OAuthToken;

            if (token == null)
            {
                var api = System.Configuration.ConfigurationManager.AppSettings["userApi"];
                var clientId = System.Configuration.ConfigurationManager.AppSettings["clientId"];
                var clientSecret = System.Configuration.ConfigurationManager.AppSettings["clientSecret"];
                var scopes = System.Configuration.ConfigurationManager.AppSettings["scopes"];

                var discovery = new DiscoveryClient(api);
                discovery.Policy.RequireHttps = false;
                var disco = await discovery.GetAsync();

                var tokenClient = new TokenClient(disco.TokenEndpoint, clientId, clientSecret);
                var tokenResponse = await tokenClient.RequestClientCredentialsAsync(scopes);

                token = new OAuthToken() { access_token = tokenResponse.AccessToken, expires_in = tokenResponse.ExpiresIn, token_type = tokenResponse.TokenType };

                CacheItemPolicy policy = new CacheItemPolicy();
                policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(2);
                _cache.Set(key, token, policy);
            }

            return token;
        }
    }
}
