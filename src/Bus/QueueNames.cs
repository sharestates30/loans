﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus
{
    public class QueueNames
    {

        public const string LoanPendingToAuthorizeQueue = "loans-loanpendingtoauthorizeQueue";
        public const string LoanPendingQueue = "loans-loanpendingQueue";
        public const string LoanAcceptedQueue = "loans-loanacceptedQueue";
        public const string LoanUnderReviewTermSheetQueue = "loans-loanunderreviewtermsheetQueue";
        public const string LoanPendingSignatureTermSheetQueue = "loans-loanpendingsignaturetermsheetQueue";
        public const string LoanSignedTermSheetQueue = "loans-loansignedtermsheetQueue";
        public const string LoanExpiredTermSheetQueue = "loans-loanexpiredtermsheetQueue";
        public const string LoanAcceptedWithDepositQueue = "loans-loanacceptedwithdepositQueue";
        public const string LoanAppraisalOrderedQueue = "loans-loanapprasailorderedQueue";
        public const string LoanAppraisalReceivedQueue = "loans-loanapprasailreceivedQueue";
        public const string LoanAppraisalReportSentQueue = "loans-loanapprasailreportsentQueue";
        public const string LoanAppraisalApprovedQueue = "loans-loanapprasailapprovedQueue";
        public const string LoanTitleRequiredQueue = "loans-loantitlerequiredQueue";
        public const string LoanUWClearQueue = "loans-loanuwclearQueue";
        public const string LoanUWTitleClearQueue = "loans-loanuwtitleclearQueue";
        public const string LoanTitleReceivedQueue = "loans-loantitlereceivedQueue";
        public const string LoanLenderInstructionsIssuedQueue = "loans-loanlenderinstructionsissuedQueue";
        public const string LoanClosedQueue = "loans-loanclosedqueue";


        public const string LoanAssignUserQueue = "loans-assignUserQueue";
        public const string LoanRejectQueue = "loans-loanrejectQueue";
        public const string LoanCommentQueue = "loans-loancommentQueue";
        public const string LoanNotificationQueue = "loans-loannotificationQueue";
        public const string LoanDocumentChangeStatusQueue = "loans-loandocumentchangestatusQueue";
        public const string LoanCreateNotificationQueue = "loans-loancreatenotificationQueue";
    }

    public class ExternalQueueNames {
        public const string NotificationEmailDelivery = "notifications-emaildelivery";
    }
}
