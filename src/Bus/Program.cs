﻿using System;
using System.Threading;
using Quartz;
using StructureMap;
using Topshelf.StructureMap;
using Topshelf.Quartz.StructureMap;
using Topshelf;
using Bus.Tasks;
using Bus.Services;

namespace Bus
{
    class Program
    {
        static void Main()
        {
            HostFactory.Run(c =>
            {
                var container = new Container(cfg =>
                {
                    cfg.For<ISendEmailService>().Use<SendEmailService>();
                    cfg.For<ITokenService>().Use<TokenService>();
                    cfg.For<ILoanResolverService>().Use<LoanResolverService>();
                });
                // Init StructureMap container 
                c.UseStructureMap(container);

                c.Service<BusServiceManager>(s =>
                {
                    //Construct topshelf service instance with StructureMap
                    s.ConstructUsingStructureMap();

                    s.WhenStarted((service, control) => service.Start());
                    s.WhenStopped((service, control) => service.Stop());

                    //Construct IJob instance with StructureMap
                    s.UseQuartzStructureMap();

                    int intervalSeconds = 10;
                    
                    s.ScheduleQuartzJob(q =>
                        q.WithJob(() => JobBuilder.Create<LoanAcceptedTask>().Build()).AddTrigger(() =>
                                TriggerBuilder.Create()
                                    .WithSimpleSchedule(builder => builder
                                                                    .WithIntervalInSeconds(intervalSeconds)
                                                                    .RepeatForever())
                                                                    .Build())
                        );

                    s.ScheduleQuartzJob(q =>
                        q.WithJob(() => JobBuilder.Create<LoanPendingToAuthorizeTask>().Build()).AddTrigger(() =>
                                TriggerBuilder.Create()
                                    .WithSimpleSchedule(builder => builder
                                                                    .WithIntervalInSeconds(intervalSeconds)
                                                                    .RepeatForever())
                                                                    .Build())
                        );

                    s.ScheduleQuartzJob(q =>
                        q.WithJob(() => JobBuilder.Create<LoanAcceptedWithDepositTask>().Build()).AddTrigger(() =>
                                TriggerBuilder.Create()
                                    .WithSimpleSchedule(builder => builder
                                                                    .WithIntervalInSeconds(intervalSeconds)
                                                                    .RepeatForever())
                                                                    .Build())
                        );
                    
                    s.ScheduleQuartzJob(q =>
                        q.WithJob(() => JobBuilder.Create<LoanAppraisalApprovedTask>().Build()).AddTrigger(() =>
                                TriggerBuilder.Create()
                                    .WithSimpleSchedule(builder => builder
                                                                    .WithIntervalInSeconds(intervalSeconds)
                                                                    .RepeatForever())
                                                                    .Build())
                        );

                    s.ScheduleQuartzJob(q =>
                       q.WithJob(() => JobBuilder.Create<LoanAppraisalOrderedTask>().Build()).AddTrigger(() =>
                               TriggerBuilder.Create()
                                   .WithSimpleSchedule(builder => builder
                                                                   .WithIntervalInSeconds(intervalSeconds)
                                                                   .RepeatForever())
                                                                   .Build())
                       );

                    s.ScheduleQuartzJob(q =>
                      q.WithJob(() => JobBuilder.Create<LoanAppraisalReceivedTask>().Build()).AddTrigger(() =>
                              TriggerBuilder.Create()
                                  .WithSimpleSchedule(builder => builder
                                                                  .WithIntervalInSeconds(intervalSeconds)
                                                                  .RepeatForever())
                                                                  .Build())
                      );

                    s.ScheduleQuartzJob(q =>
                      q.WithJob(() => JobBuilder.Create<LoanAppraisalReportSentTask>().Build()).AddTrigger(() =>
                              TriggerBuilder.Create()
                                  .WithSimpleSchedule(builder => builder
                                                                  .WithIntervalInSeconds(intervalSeconds)
                                                                  .RepeatForever())
                                                                  .Build())
                      );

                    s.ScheduleQuartzJob(q =>
                      q.WithJob(() => JobBuilder.Create<LoanCommentTask>().Build()).AddTrigger(() =>
                              TriggerBuilder.Create()
                                  .WithSimpleSchedule(builder => builder
                                                                  .WithIntervalInSeconds(intervalSeconds)
                                                                  .RepeatForever())
                                                                  .Build())
                      );

                    s.ScheduleQuartzJob(q =>
                      q.WithJob(() => JobBuilder.Create<LoanNotificationTask>().Build()).AddTrigger(() =>
                              TriggerBuilder.Create()
                                  .WithSimpleSchedule(builder => builder
                                                                  .WithIntervalInSeconds(intervalSeconds)
                                                                  .RepeatForever())
                                                                  .Build())
                      );

                    s.ScheduleQuartzJob(q =>
                      q.WithJob(() => JobBuilder.Create<LoanExpiredTermSheetTask>().Build()).AddTrigger(() =>
                              TriggerBuilder.Create()
                                  .WithSimpleSchedule(builder => builder
                                                                  .WithIntervalInSeconds(intervalSeconds)
                                                                  .RepeatForever())
                                                                  .Build())
                      );

                    s.ScheduleQuartzJob(q =>
                      q.WithJob(() => JobBuilder.Create<LoanPendingTask>().Build()).AddTrigger(() =>
                              TriggerBuilder.Create()
                                  .WithSimpleSchedule(builder => builder
                                                                  .WithIntervalInSeconds(intervalSeconds)
                                                                  .RepeatForever())
                                                                  .Build())
                      );

                    s.ScheduleQuartzJob(q =>
                      q.WithJob(() => JobBuilder.Create<LoanRejectTask>().Build()).AddTrigger(() =>
                              TriggerBuilder.Create()
                                  .WithSimpleSchedule(builder => builder
                                                                  .WithIntervalInSeconds(intervalSeconds)
                                                                  .RepeatForever())
                                                                  .Build())
                      );

                    s.ScheduleQuartzJob(q =>
                      q.WithJob(() => JobBuilder.Create<LoanSignedTermSheetTask>().Build()).AddTrigger(() =>
                              TriggerBuilder.Create()
                                  .WithSimpleSchedule(builder => builder
                                                                  .WithIntervalInSeconds(intervalSeconds)
                                                                  .RepeatForever())
                                                                  .Build())
                      );


                    s.ScheduleQuartzJob(q =>
                      q.WithJob(() => JobBuilder.Create<LoanUnderReviewTermSheetTask>().Build()).AddTrigger(() =>
                              TriggerBuilder.Create()
                                  .WithSimpleSchedule(builder => builder
                                                                  .WithIntervalInSeconds(intervalSeconds)
                                                                  .RepeatForever())
                                                                  .Build())
                      );

                    s.ScheduleQuartzJob(q =>
                     q.WithJob(() => JobBuilder.Create<LoanPendingSignatureTermSheetTask>().Build()).AddTrigger(() =>
                             TriggerBuilder.Create()
                                 .WithSimpleSchedule(builder => builder
                                                                 .WithIntervalInSeconds(intervalSeconds)
                                                                 .RepeatForever())
                                                                 .Build())
                     );

                    s.ScheduleQuartzJob(q =>
                     q.WithJob(() => JobBuilder.Create<LoanAssignedUserTask>().Build()).AddTrigger(() =>
                             TriggerBuilder.Create()
                                 .WithSimpleSchedule(builder => builder
                                                                 .WithIntervalInSeconds(intervalSeconds)
                                                                 .RepeatForever())
                                                                 .Build())
                     );

                    s.ScheduleQuartzJob(q =>
                     q.WithJob(() => JobBuilder.Create<LoanClosedTask>().Build()).AddTrigger(() =>
                             TriggerBuilder.Create()
                                 .WithSimpleSchedule(builder => builder
                                                                 .WithIntervalInSeconds(intervalSeconds)
                                                                 .RepeatForever())
                                                                 .Build())
                     );

                    s.ScheduleQuartzJob(q =>
                    q.WithJob(() => JobBuilder.Create<LoanDocumentChangeStatusTask>().Build()).AddTrigger(() =>
                            TriggerBuilder.Create()
                                .WithSimpleSchedule(builder => builder
                                                                .WithIntervalInSeconds(intervalSeconds)
                                                                .RepeatForever())
                                                                .Build())
                    );

                    s.ScheduleQuartzJob(q =>
                    q.WithJob(() => JobBuilder.Create<LoanCreateNotificationTask>().Build()).AddTrigger(() =>
                            TriggerBuilder.Create()
                                .WithSimpleSchedule(builder => builder
                                                                .WithIntervalInSeconds(intervalSeconds)
                                                                .RepeatForever())
                                                                .Build())
                    );
                });

                c.SetServiceName("ShareStates.Loans.Bus");
                c.SetDisplayName("ShareStates Loans Bus");
                c.SetDescription("ShareStates Loans Bus");
                c.StartAutomatically();
                
            });
        }
    }
}
