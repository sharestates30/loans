﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Messages
{
    public class UserAssignedMessage
    {
        public Guid LoanId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
}
