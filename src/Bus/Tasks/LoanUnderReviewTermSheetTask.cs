﻿using Bus.Messages;
using Bus.Models;
using Bus.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Bus.Tasks
{
    public class LoanUnderReviewTermSheetTask : IJob
    {
        private readonly CloudDataService _cloudDataService;
        private readonly ITokenService _tokenService;

        public LoanUnderReviewTermSheetTask(CloudDataService cloudDataService, ITokenService tokenService)
        {
            this._cloudDataService = cloudDataService;
            this._tokenService = tokenService;
        }

        public void Execute(IJobExecutionContext context)
        {
            var queue = this._cloudDataService.GetQueue(QueueNames.LoanUnderReviewTermSheetQueue);
            var claim = this._cloudDataService.ReceiveMessage(queue).GetAwaiter().GetResult();

            this._cloudDataService.ProcessMessage<LoanMessage>(queue, claim, (message) =>
            {
                // Get token
                var token = this._tokenService.GetToken().GetAwaiter().GetResult();

                // Get api url
                var signApi = System.Configuration.ConfigurationManager.AppSettings["signApi"];
                var storageApi = System.Configuration.ConfigurationManager.AppSettings["storageApi"];
                var loanApi = System.Configuration.ConfigurationManager.AppSettings["loanApi"];
                var developerApi = System.Configuration.ConfigurationManager.AppSettings["developerApi"];

                var httpClient = new HttpClient();
                httpClient.SetBearerToken(token.access_token);

                // Send term sheet.
                var loan = httpClient.GetAsync($"{loanApi}/loan/{message.LoanId}").GetAwaiter().GetResult().ReadAsObject<LoanViewModel>();
                var loanTermSheet = httpClient.GetAsync($"{loanApi}/loan/{message.LoanId}/termSheet").GetAwaiter().GetResult().ReadAsObject<LoanTermSheetViewModel>();
                var developer = httpClient.GetAsync($"{developerApi}/developer/{message.DeveloperId}").GetAwaiter().GetResult().ReadAsObject<DeveloperViewModel>();
                var parameters = httpClient.GetAsync($"{loanApi}/parameter?code=TermSheetExpire").GetAwaiter().GetResult().ReadAsObject<IReadOnlyList<ParameterViewModel>>();

                int expireDayValue = 0;
                int.TryParse(parameters.FirstOrDefault().Value, out expireDayValue);

                var expireDateTime = DateTime.UtcNow.AddDays(expireDayValue);

                var symbol = "$";
                
                var request = new {
                    UserId = developer.DeveloperId,
                    Email = developer.Email,
                    FirstName = developer.FirstName,
                    LastName = developer.LastName,
                    DocumentName = $"Term-Sheet{loan.LoanNumber}-{developer.FirstName}-{developer.LastName}",
                    Message = "Term Sheet",
                    LoanId = message.LoanId,
                    ExpireDate = expireDateTime,
                    Lender = loanTermSheet.Lender,
                    LoanPurposeType = loanTermSheet.LoanPurposeTypeName,
                    Property = loanTermSheet.Property,
                    Term = loanTermSheet.LoanTermRequest,
                    ExtensionOptions = loanTermSheet.ExtensionOptions,
                    EarlyPayoff = loanTermSheet.EarlyPayoff,
                    Assumability = $"{symbol}{loanTermSheet.Assumability}",
                    Rate = loanTermSheet.Rate,
                    Points = loanTermSheet.Points,
                    LoanAmount = $"{symbol}{loanTermSheet.LoanRequest1}",
                    InterestAccrual = $"{symbol}{loanTermSheet.InterestAccrual}",
                    PurchasePrice = $"{symbol}{loanTermSheet.PurchasePrice}",
                    AmountExistingLiens = $"{symbol}{loanTermSheet.AmountExistingLiens}",
                    PurposeRefinance = $"{symbol}{loanTermSheet.PurchasePrice}",
                    DownPayment = $"{symbol}{loanTermSheet.DownPayment}",
                    RehabCost = $"{symbol}{loanTermSheet.RehabCost}",
                    PercentageRehabCostFinanced = $"{symbol}{loanTermSheet.PercentageRehabCostFinanced}",
                    RehabCostFinanced = $"{symbol}{loanTermSheet.RehabCostFinanced}",
                    TotalProjectCost = $"{symbol}{loanTermSheet.LoanRequest1}",
                    LoanToValue = $"{symbol}{loanTermSheet.LTV}",
                    InvestmentsOriginationPoints = $"{symbol}{loanTermSheet.InvestmentsOriginationPoints}",
                    BrokerFee = $"{symbol}{loanTermSheet.BrokerFee}",
                    BankAttorneyFee = $"{symbol}{loanTermSheet.BankAttorneyFee}",
                    ConstructionDraws = $"{symbol}{loanTermSheet.ConstructionDraws}",
                    EstimatedAppraisal = $"{symbol}{loanTermSheet.EstimatedAppraisal}",
                    TermSheetBinding = $"{symbol}{loanTermSheet.TermSheetBinding}",
                    EstimatedTitleClosingCosts = $"{symbol}{loanTermSheet.EstimatedTitleClosingCosts}",
                    PercentageClosingCostsFinanced = $"{symbol}{loanTermSheet.PercentageClosingCostsFinanced}",
                    ClosingCostsFinanced = $"{symbol}{loanTermSheet.ClosingCostsFinanced}",
                    InterestReserve = $"{symbol}{loanTermSheet.InterestReserve}",
                    RefinanceClosingCostsFinanced = $"{symbol}{loanTermSheet.RefinanceClosingCostsFinanced}",
                    LoanAmountMayNotExceedARV = $"{symbol}{loanTermSheet.LoanAmountMayNotExceedARV}",
                    AsIsValueNeedfromSharestatesOrderedAppraiser = $"{symbol}{loanTermSheet.AsIsValueNeedfromSharestatesOrderedAppraiser}",
                    LoanAmountMayNotExceedAsIsValue = $"{symbol}{loanTermSheet.LoanAmountMayNotExceedAsIsValue}",
                    Guarantor = loanTermSheet.Guarantor,
                    BorrowerForm = loanTermSheet.BorrowerForm,
                    Payments = loanTermSheet.Payments,
                    SingleEntity = loanTermSheet.SingleEntity,
                    CrossDefault = loanTermSheet.CrossDefault,
                    Collateral = loanTermSheet.Collateral,
                    SecondaryFinance = loanTermSheet.SecondaryFinance,
                    TitleInsurance = loanTermSheet.TitleInsurance,
                    PowerAttorney = loanTermSheet.PowerAttorney,
                    HazardInsurance = loanTermSheet.HazardInsurance,
                };

                var response = httpClient.PostAsync($"{signApi}/sign/termsheet", new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json")).GetAwaiter().GetResult();

                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.Content.ReadAsStringAsync().Result);

                var signData = response.ReadAsObject<SignTermSheetViewModel>();
                
                var queuePendingSignature = this._cloudDataService.GetQueue(QueueNames.LoanPendingSignatureTermSheetQueue);

                dynamic jsonObject = new JObject();
                jsonObject.LoanId = message.LoanId;
                jsonObject.FileName = signData.FileName;
                jsonObject.ActivityId = signData.ActivityId;
                jsonObject.DeveloperId = developer.DeveloperId.ToString();

                this._cloudDataService.SendMessage(queuePendingSignature, jsonObject).GetAwaiter().GetResult();

            }).GetAwaiter().GetResult();
        }
    }
}

