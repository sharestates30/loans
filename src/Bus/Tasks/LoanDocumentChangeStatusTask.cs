﻿using Bus.Messages;
using Bus.Models;
using Bus.Services;
using Quartz;
using Sharestates.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Bus.Tasks
{
    public class LoanDocumentChangeStatusTask : IJob
    {

        private readonly CloudDataService _cloudDataService;
        private readonly ITokenService _tokenService;
        private readonly ILoanResolverService _loanResolver;
        private readonly ISendEmailService _sendEmailService;

        public LoanDocumentChangeStatusTask(CloudDataService cloudDataService, ITokenService tokenService, ILoanResolverService loanResolver, ISendEmailService sendEmailService)
        {
            this._cloudDataService = cloudDataService;
            this._tokenService = tokenService;
            this._loanResolver = loanResolver;
            this._sendEmailService = sendEmailService;
        }

        public void Execute(IJobExecutionContext context)
        {
            var queue = this._cloudDataService.GetQueue(QueueNames.LoanDocumentChangeStatusQueue);
            var claim = this._cloudDataService.ReceiveMessage(queue).GetAwaiter().GetResult();

            this._cloudDataService.ProcessMessage<LoanDocumentChangeStatusMessage>(queue, claim, (message) =>
            {
                // Get token
                var token = this._tokenService.GetToken().GetAwaiter().GetResult();

                var loan = this._loanResolver.GetLoan(message.LoanId, token).GetAwaiter().GetResult();

                // notify to borrower and admin.
                var emails = new List<RecipientViewModel>();
                emails.Add(new RecipientViewModel { Email = loan.Developer.Email,  FirstName = loan.Developer.FirstName, LastName = loan.Developer.LastName });

                if (loan.Broker != null)
                    emails.Add(new RecipientViewModel { Email = loan.Broker.Email, FirstName = loan.Broker.FirstName, LastName = loan.Broker.LastName });

                var model = new List<DataModelMessage>();
                model.Add(new DataModelMessage { Key = "statusName", Value = message.StatusName });

                this._sendEmailService.Send(emails, model, "LoanDocumentChangeStatus").GetAwaiter().GetResult();


            }).GetAwaiter().GetResult();

        }
    }
}
