﻿using Bus.Messages;
using Bus.Models;
using Bus.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Bus.Tasks
{
    public class LoanAppraisalReceivedTask : IJob
    {

        private readonly CloudDataService _cloudDataService;
        private readonly ITokenService _tokenService;
        private readonly ILoanResolverService _loanResolver;
        private readonly ISendEmailService _sendEmailService;

        public LoanAppraisalReceivedTask(CloudDataService cloudDataService, ITokenService tokenService, ILoanResolverService loanResolver, ISendEmailService sendEmailService)
        {
            this._cloudDataService = cloudDataService;
            this._tokenService = tokenService;
            this._loanResolver = loanResolver;
            this._sendEmailService = sendEmailService;

        }

        public void Execute(IJobExecutionContext context)
        {
            var queue = this._cloudDataService.GetQueue(QueueNames.LoanAppraisalReceivedQueue);
            var claim = this._cloudDataService.ReceiveMessage(queue).GetAwaiter().GetResult();

            this._cloudDataService.ProcessMessage<LoanMessage>(queue, claim, (message) =>
            {

                // Get token
                var token = this._tokenService.GetToken().GetAwaiter().GetResult();

                // Get api url
                var loanApi = System.Configuration.ConfigurationManager.AppSettings["loanApi"];

                var loan = this._loanResolver.GetLoan(message.LoanId, token).GetAwaiter().GetResult();

                // notify
                var emails = new List<RecipientViewModel>();

                if (loan.Developer != null)
                    emails.Add(new RecipientViewModel { Email = loan.Developer.Email, FirstName = loan.Developer.FirstName, LastName = loan.Developer.LastName });

                if (loan.Broker != null)
                    emails.Add(new RecipientViewModel { Email = loan.Broker.Email, FirstName = loan.Broker.FirstName, LastName = loan.Broker.LastName });

                var model = new List<DataModelMessage>();

                this._sendEmailService.Send(emails, model, "ProjectChangedToAppraisalReceived").GetAwaiter().GetResult();

            }).GetAwaiter().GetResult();

        }
    }
}
