﻿using Bus.Messages;
using Bus.Models;
using Bus.Services;
using Newtonsoft.Json;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Bus.Tasks
{
    public class LoanSignedTermSheetTask : IJob
    {
        private readonly CloudDataService _cloudDataService;
        private readonly ISendEmailService _sendEmailService;
        private readonly ITokenService _tokenService;
        private readonly ILoanResolverService _loanResolver;

        public LoanSignedTermSheetTask(CloudDataService cloudDataService, ISendEmailService sendEmailService, ITokenService tokenService, ILoanResolverService loanResolver)
        {
            this._cloudDataService = cloudDataService;
            this._sendEmailService = sendEmailService;
            this._tokenService = tokenService;
            this._loanResolver = loanResolver;
        }

        public void Execute(IJobExecutionContext context)
        {
            var queue = this._cloudDataService.GetQueue(QueueNames.LoanSignedTermSheetQueue);
            var claim = this._cloudDataService.ReceiveMessage(queue).GetAwaiter().GetResult();

            this._cloudDataService.ProcessMessage<LoanSignedTermSheetMessage>(queue, claim, (message) =>
            {
                // Get token
                var token = this._tokenService.GetToken().GetAwaiter().GetResult();

                // Get api url
                var loanApi = System.Configuration.ConfigurationManager.AppSettings["loanApi"];
                var signApi = System.Configuration.ConfigurationManager.AppSettings["signApi"];
                var storageApi = System.Configuration.ConfigurationManager.AppSettings["storageApi"];

                var httpClient = new HttpClient();
                httpClient.SetBearerToken(token.access_token);

                var loan = this._loanResolver.GetLoan(message.LoanId, token).GetAwaiter().GetResult();

                // upload file to storage.
                var responseFile = httpClient.GetAsync($"{signApi}/sign/termsheet/signed/download/{message.FileName}").GetAwaiter().GetResult();
                var fileBytes = responseFile.Content.ReadAsByteArrayAsync().GetAwaiter().GetResult();
                MultipartFormDataContent formData = new MultipartFormDataContent();
                formData.Add(new StreamContent(new MemoryStream(fileBytes)), "files", message.FileName);
                formData.Add(new StringContent("UserId"), message.DeveloperId);

                var responseFiles = httpClient.PostAsync($"{storageApi}/file", formData).GetAwaiter().GetResult();
                var files = responseFiles.ReadAsObject<IReadOnlyList<FileViewModel>>();

                var request = new
                {
                    TermSheetSignedFileId = files.ElementAt(0).FileItemId.ToString(),
                    TermSheetSignedFileName = files.ElementAt(0).FileName,
                    TermSheetSignedFileLink = files.ElementAt(0).FileUri,
                };

                // Mark loan to Term Sheet Signed
                var response = httpClient.PutAsync($"{loanApi}/loan/{message.LoanId}/termSheetSigned", new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json")).GetAwaiter().GetResult();

                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.Content.ReadAsStringAsync().Result);

                // notify
                var emails = new List<RecipientViewModel>();

                if (loan.Developer != null)
                    emails.Add(new RecipientViewModel { Email = loan.Developer.Email, FirstName = loan.Developer.FirstName, LastName = loan.Developer.LastName });

                if (loan.Broker != null)
                    emails.Add(new RecipientViewModel { Email = loan.Broker.Email, FirstName = loan.Broker.FirstName, LastName = loan.Broker.LastName });

                var modelTermSheet = new List<DataModelMessage>();
                var modelCreditCardAuth = new List<DataModelMessage>();
                modelCreditCardAuth.Add(new DataModelMessage() { Key = "loanNumber", Value = loan.Loan.LoanNumber });
                modelCreditCardAuth.Add(new DataModelMessage() { Key = "loanId", Value = loan.Loan.LoanId });
                modelCreditCardAuth.Add(new DataModelMessage() { Key = "firstName", Value = loan.Developer.FirstName });
                modelCreditCardAuth.Add(new DataModelMessage() { Key = "lastName", Value = loan.Developer.LastName });

                this._sendEmailService.Send(emails, modelTermSheet, "TermSheetAccepted").GetAwaiter().GetResult();
                this._sendEmailService.Send(emails, modelCreditCardAuth, "CreditCardAuthorizationForm").GetAwaiter().GetResult();


            }).GetAwaiter().GetResult();
        }
    }
}
