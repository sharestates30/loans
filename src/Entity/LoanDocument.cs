﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class LoanDocument : BaseEntity
    {
        public long LoanDocumentId { get; set; }

        public int DocumentId { get; set; }
        public virtual Document Document { get; set; }

        public Guid LoanId { get; set; }
        public virtual Loan Loan { get; set; }
        
        public DocumentStatusTypeEnum Status { get; set; }

        public virtual ICollection<LoanDocumentFile> Files { get; set; }

        public void AddFile(string fileId, string fileName, string fileLink, string createdBy, string createdByName) {
            if (this.Files == null) {
                this.Files = new List<LoanDocumentFile>();
            }

            this.Files.Add(new LoanDocumentFile { FileId = fileId, FileName = fileName, FileLink = fileLink, CreatedBy = createdBy, CreatedByName = createdByName, CreatedOn = DateTime.UtcNow });
        }

    }
}
