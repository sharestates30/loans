﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public enum DocumentStatusTypeEnum
    {
        Sent,
        Partial,
        Completed,
        Pending,
        Cleared
    }
}
