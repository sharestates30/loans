﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class LoanDocumentFile : BaseEntity
    {
        public long LoanDocumentFileId { get; set; }

        public LoanDocumentFile() : base()
        {

        }

        public LoanDocumentFile(string fileId, string fileName, string fileLink) : this()
        {
            this.FileId = fileId;
            this.FileName = fileName;
            this.FileLink = fileLink;
        }

        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FileLink { get; set; }

        public long LoanDocumentId { get; set; }
        public virtual LoanDocument LoanDocument { get; set; }
    }
}
