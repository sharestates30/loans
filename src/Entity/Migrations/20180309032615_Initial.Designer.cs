﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Entity;

namespace Entity.Migrations
{
    [DbContext(typeof(LoanDbContext))]
    [Migration("20180309032615_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.5")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Entity.Document", b =>
                {
                    b.Property<int>("DocumentId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AllowShared");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Description");

                    b.Property<string>("DocumentCode");

                    b.Property<int>("InternalDocument");

                    b.Property<string>("Name");

                    b.Property<bool>("NeedValidation");

                    b.Property<int>("Order");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("DocumentId");

                    b.ToTable("Documents");
                });

            modelBuilder.Entity("Entity.DocumentStage", b =>
                {
                    b.Property<int>("DocumentStageId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int>("DocumentId");

                    b.Property<int>("StageId");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("DocumentStageId");

                    b.HasIndex("DocumentId");

                    b.HasIndex("StageId");

                    b.ToTable("DocumentStages");
                });

            modelBuilder.Entity("Entity.Insurance", b =>
                {
                    b.Property<Guid>("LoanId");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<decimal?>("FloodInsuranceAmount");

                    b.Property<DateTime?>("FloodInsuranceGoodThroughDate");

                    b.Property<string>("FloodInsurancePaidReceiptFileId");

                    b.Property<string>("FloodInsurancePaidReceiptFileLink");

                    b.Property<string>("FloodInsurancePaidReceiptFileName");

                    b.Property<string>("FloodInsurancePolicyFileId");

                    b.Property<string>("FloodInsurancePolicyFileLink");

                    b.Property<string>("FloodInsurancePolicyFileName");

                    b.Property<decimal?>("HazardInsuranceAmount");

                    b.Property<DateTime?>("HazardInsuranceGoodThroughDate");

                    b.Property<string>("HazardInsurancePaidReceiptFileId");

                    b.Property<string>("HazardInsurancePaidReceiptFileLink");

                    b.Property<string>("HazardInsurancePaidReceiptFileName");

                    b.Property<string>("HazardInsurancePolicyFileId");

                    b.Property<string>("HazardInsurancePolicyFileLink");

                    b.Property<string>("HazardInsurancePolicyFileName");

                    b.Property<bool>("IsFloodInsuranceRequired");

                    b.Property<bool>("IsHazardInsuranceRequired");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("LoanId");

                    b.ToTable("Insurances");
                });

            modelBuilder.Entity("Entity.Loan", b =>
                {
                    b.Property<Guid>("LoanId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("Address2");

                    b.Property<string>("AppraisalCompanyName");

                    b.Property<DateTime?>("AppraisalDateNeeded");

                    b.Property<DateTime?>("AppraisalDateOrdered");

                    b.Property<string>("AppraisalStatus");

                    b.Property<decimal?>("AppraiserFee");

                    b.Property<Guid?>("AppraiserId");

                    b.Property<string>("AppraiserName");

                    b.Property<decimal>("AquisitionLoanRequest");

                    b.Property<decimal>("AsIsValue");

                    b.Property<string>("AssignorName");

                    b.Property<string>("BankruptcyExplanation");

                    b.Property<int>("BankruptcyYears");

                    b.Property<int>("BorrowerCreditScore");

                    b.Property<string>("BorrowerEntityName");

                    b.Property<Guid?>("BorrowerId");

                    b.Property<string>("BorrowerName");

                    b.Property<string>("BrokerAgentName");

                    b.Property<string>("BrokerEntityName");

                    b.Property<Guid?>("BrokerId");

                    b.Property<string>("City");

                    b.Property<DateTime?>("ClosingDate");

                    b.Property<DateTime?>("ClosingTime");

                    b.Property<decimal>("ConstructionBudget");

                    b.Property<decimal>("ConstructionLoanRequest");

                    b.Property<decimal?>("ContractorAssignedValue");

                    b.Property<string>("Country");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int>("CurrentAssetType");

                    b.Property<int>("CurrentAssetTypeValue");

                    b.Property<string>("DefaultedLoanExplanation");

                    b.Property<DateTime>("DesiredFundingDate");

                    b.Property<int>("DevelopmentPhase");

                    b.Property<decimal>("DisbursedAmountatClosing");

                    b.Property<Guid?>("EntityId");

                    b.Property<string>("EntityName");

                    b.Property<decimal>("EscrowHoldBack");

                    b.Property<int>("ExitStrategy");

                    b.Property<string>("FiledForeclosureExplanation");

                    b.Property<decimal>("FinalValue");

                    b.Property<bool>("HasAssignmentFlipType");

                    b.Property<bool>("HasAssignorAffiliate");

                    b.Property<bool>("HasBankruptcyType");

                    b.Property<bool>("HasDefaultedLoanType");

                    b.Property<bool>("HasFiledForeclosure");

                    b.Property<bool>("HasOutstandingJudgmentsType");

                    b.Property<bool>("HasRehabComponent");

                    b.Property<bool>("HasSalesRepresentative");

                    b.Property<bool>("IsApproved");

                    b.Property<bool>("IsLLCTransactionType");

                    b.Property<bool>("IsPipeline");

                    b.Property<bool>("IsShortSaleTransactionType");

                    b.Property<bool>("IsTOETransaction");

                    b.Property<string>("LoanNumber");

                    b.Property<int>("LoanPurposeType");

                    b.Property<int>("LoanTermRequest");

                    b.Property<string>("Location");

                    b.Property<int?>("MoreTermValue");

                    b.Property<bool>("NeedMoreTerm");

                    b.Property<int>("NewAssetType");

                    b.Property<int>("NewAssetTypeValue");

                    b.Property<int>("Occupancy");

                    b.Property<DateTime?>("OriginalPurchaseDate");

                    b.Property<decimal?>("OriginalPurchasePrice");

                    b.Property<string>("OriginationEntity");

                    b.Property<string>("OutstandingJudgmentsExplanation");

                    b.Property<string>("ProcessStatus");

                    b.Property<decimal>("PurchasePrice");

                    b.Property<string>("ReasonRejected");

                    b.Property<string>("SalesPersonId");

                    b.Property<string>("SalesPersonName");

                    b.Property<string>("State");

                    b.Property<DateTime?>("SubmittedDay");

                    b.Property<DateTime?>("TOEClosingDate");

                    b.Property<DateTime?>("TargetClosingDate");

                    b.Property<string>("TermSheetStatus");

                    b.Property<int>("TimeOfClosing");

                    b.Property<string>("TitleBusinessName");

                    b.Property<string>("TitleCompanyFirstName");

                    b.Property<Guid?>("TitleCompanyId");

                    b.Property<string>("TitleCompanyLastName");

                    b.Property<string>("TitlePropertyNumber");

                    b.Property<DateTime?>("TitleReportBackDate");

                    b.Property<DateTime?>("TitleReportOrderedDate");

                    b.Property<string>("TitleReportStatus");

                    b.Property<int>("TransactionType");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.Property<string>("ZipCode");

                    b.Property<string>("ssignorAffiliateExplanation");

                    b.HasKey("LoanId");

                    b.ToTable("Loans");
                });

            modelBuilder.Entity("Entity.LoanAuction", b =>
                {
                    b.Property<Guid>("LoanAuctionId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Awarded");

                    b.Property<decimal>("Bid");

                    b.Property<string>("Content");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int>("LoanAuctionType");

                    b.Property<Guid>("LoanId");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.Property<Guid>("VendorId");

                    b.Property<string>("VendorName");

                    b.HasKey("LoanAuctionId");

                    b.HasIndex("LoanId");

                    b.ToTable("Auctions");
                });

            modelBuilder.Entity("Entity.LoanDocument", b =>
                {
                    b.Property<long>("LoanDocumentId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int>("DocumentId");

                    b.Property<Guid>("LoanId");

                    b.Property<int>("Status");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("LoanDocumentId");

                    b.HasIndex("DocumentId");

                    b.HasIndex("LoanId");

                    b.ToTable("LoanDocuments");
                });

            modelBuilder.Entity("Entity.LoanDocumentFile", b =>
                {
                    b.Property<long>("LoanDocumentFileId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("FileId");

                    b.Property<string>("FileLink");

                    b.Property<string>("FileName");

                    b.Property<long>("LoanDocumentId");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("LoanDocumentFileId");

                    b.HasIndex("LoanDocumentId");

                    b.ToTable("LoanDocumentFiles");
                });

            modelBuilder.Entity("Entity.LoanLog", b =>
                {
                    b.Property<long>("LoanLogId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<Guid>("LoanId");

                    b.Property<string>("NewData");

                    b.Property<string>("OldData");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("LoanLogId");

                    b.HasIndex("LoanId");

                    b.ToTable("LoanLog");
                });

            modelBuilder.Entity("Entity.LoanTracking", b =>
                {
                    b.Property<int>("LoanTrackingId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<string>("Content");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<Guid>("LoanId");

                    b.Property<string>("Status");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("LoanTrackingId");

                    b.HasIndex("LoanId");

                    b.ToTable("LoanTrackings");
                });

            modelBuilder.Entity("Entity.LoanTransaction", b =>
                {
                    b.Property<int>("LoanTransactionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<Guid>("LoanId");

                    b.Property<int?>("LoanTransactionId1");

                    b.Property<string>("Message");

                    b.Property<DateTime>("TransactionDate");

                    b.Property<string>("TransactionId");

                    b.Property<string>("TransactionStatus");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("LoanTransactionId");

                    b.HasIndex("LoanId");

                    b.HasIndex("LoanTransactionId1");

                    b.ToTable("LoanTransactions");
                });

            modelBuilder.Entity("Entity.Parameter", b =>
                {
                    b.Property<int>("ParameterId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<bool>("IsActive");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("ParameterId");

                    b.ToTable("Parameters");
                });

            modelBuilder.Entity("Entity.Rol", b =>
                {
                    b.Property<int>("RolId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("RolId");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("Entity.Stage", b =>
                {
                    b.Property<int>("StageId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<string>("StageCode");

                    b.Property<int>("StageGroupId");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("StageId");

                    b.HasIndex("StageGroupId");

                    b.ToTable("Stages");
                });

            modelBuilder.Entity("Entity.StageGroup", b =>
                {
                    b.Property<int>("StageGroupId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<string>("StageGroupCode");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("StageGroupId");

                    b.ToTable("StageGroups");
                });

            modelBuilder.Entity("Entity.StageRole", b =>
                {
                    b.Property<int>("StageRoleId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int?>("RolId");

                    b.Property<int>("RoleId");

                    b.Property<int>("StageId");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("StageRoleId");

                    b.HasIndex("RolId");

                    b.HasIndex("StageId");

                    b.ToTable("StageRoles");
                });

            modelBuilder.Entity("Entity.StageStatus", b =>
                {
                    b.Property<int>("StageStatusId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ProcessStatus");

                    b.Property<int>("StageId");

                    b.HasKey("StageStatusId");

                    b.HasIndex("StageId");

                    b.ToTable("StageStatus");
                });

            modelBuilder.Entity("Entity.TermSheet", b =>
                {
                    b.Property<Guid>("LoanId");

                    b.Property<decimal>("AmountExistingLiens");

                    b.Property<decimal>("AsIsValueNeedfromSharestatesOrderedAppraiser");

                    b.Property<decimal>("Assumability");

                    b.Property<decimal>("BankAttorneyFee");

                    b.Property<string>("BorrowerForm");

                    b.Property<decimal>("BrokerFee");

                    b.Property<decimal>("ClosingCostsFinanced");

                    b.Property<string>("Collateral");

                    b.Property<decimal>("ConstructionDraws");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("CrossDefault");

                    b.Property<decimal>("DownPayment");

                    b.Property<decimal>("EarlyPayoff");

                    b.Property<decimal>("EstimatedAppraisal");

                    b.Property<decimal>("EstimatedTitleClosingCosts");

                    b.Property<string>("ExtensionOptions");

                    b.Property<string>("Guarantor");

                    b.Property<string>("HazardInsurance");

                    b.Property<decimal>("InterestAccrual");

                    b.Property<decimal>("InterestReserve");

                    b.Property<decimal>("InvestmentsOriginationPoints");

                    b.Property<decimal>("LTV");

                    b.Property<string>("Lender");

                    b.Property<decimal>("LoanAmountMayNotExceedARV");

                    b.Property<decimal>("LoanAmountMayNotExceedAsIsValue");

                    b.Property<decimal>("Payments");

                    b.Property<decimal>("PercentageClosingCostsFinanced");

                    b.Property<decimal>("PercentageRehabCostFinanced");

                    b.Property<decimal>("Points");

                    b.Property<string>("PowerAttorney");

                    b.Property<decimal>("PurposeRefinance");

                    b.Property<decimal>("Rate");

                    b.Property<decimal>("RefinanceClosingCostsFinanced");

                    b.Property<decimal>("RehabCost");

                    b.Property<decimal>("RehabCostFinanced");

                    b.Property<string>("SecondaryFinance");

                    b.Property<string>("SingleEntity");

                    b.Property<decimal>("TermSheetBinding");

                    b.Property<string>("TitleInsurance");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("LoanId");

                    b.ToTable("TermSheets");
                });

            modelBuilder.Entity("Entity.UserRol", b =>
                {
                    b.Property<Guid>("UserRolId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int>("RolId");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.Property<string>("UserName");

                    b.HasKey("UserRolId");

                    b.HasIndex("RolId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Entity.DocumentStage", b =>
                {
                    b.HasOne("Entity.Document", "Document")
                        .WithMany("Stages")
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entity.Stage", "Stage")
                        .WithMany("Documents")
                        .HasForeignKey("StageId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.Insurance", b =>
                {
                    b.HasOne("Entity.Loan", "Loan")
                        .WithOne("Insurance")
                        .HasForeignKey("Entity.Insurance", "LoanId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.LoanAuction", b =>
                {
                    b.HasOne("Entity.Loan", "Loan")
                        .WithMany("Auctions")
                        .HasForeignKey("LoanId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.LoanDocument", b =>
                {
                    b.HasOne("Entity.Document", "Document")
                        .WithMany("LoanDocuments")
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entity.Loan", "Loan")
                        .WithMany("Documents")
                        .HasForeignKey("LoanId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.LoanDocumentFile", b =>
                {
                    b.HasOne("Entity.LoanDocument", "LoanDocument")
                        .WithMany("Files")
                        .HasForeignKey("LoanDocumentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.LoanLog", b =>
                {
                    b.HasOne("Entity.Loan", "Loan")
                        .WithMany("Logs")
                        .HasForeignKey("LoanId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.LoanTracking", b =>
                {
                    b.HasOne("Entity.Loan", "Loan")
                        .WithMany("Trackings")
                        .HasForeignKey("LoanId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.LoanTransaction", b =>
                {
                    b.HasOne("Entity.Loan")
                        .WithMany("Transactions")
                        .HasForeignKey("LoanId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entity.LoanTransaction", "Loan")
                        .WithMany()
                        .HasForeignKey("LoanTransactionId1");
                });

            modelBuilder.Entity("Entity.Stage", b =>
                {
                    b.HasOne("Entity.StageGroup", "StageGroup")
                        .WithMany("Stages")
                        .HasForeignKey("StageGroupId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.StageRole", b =>
                {
                    b.HasOne("Entity.Rol", "Rol")
                        .WithMany()
                        .HasForeignKey("RolId");

                    b.HasOne("Entity.Stage", "Stage")
                        .WithMany("Roles")
                        .HasForeignKey("StageId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.StageStatus", b =>
                {
                    b.HasOne("Entity.Stage", "Stage")
                        .WithMany("Status")
                        .HasForeignKey("StageId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.TermSheet", b =>
                {
                    b.HasOne("Entity.Loan", "Loan")
                        .WithOne("TermSheet")
                        .HasForeignKey("Entity.TermSheet", "LoanId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entity.UserRol", b =>
                {
                    b.HasOne("Entity.Rol", "Rol")
                        .WithMany("Users")
                        .HasForeignKey("RolId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
