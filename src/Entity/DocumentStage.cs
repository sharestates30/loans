﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class DocumentStage : BaseEntity
    {
        public int DocumentStageId { get; set; }

        public int StageId { get; set; }
        public virtual Stage Stage { get; set; }

        public int DocumentId { get; set; }
        public virtual Document Document { get; set; }
    }
}
