﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class LoanTracking : BaseEntity
    {
        public int LoanTrackingId { get; set; }
        public Guid LoanId { get; set; }
        public virtual Loan Loan { get; set; }
        public string Status { get; set; }
        public string Code { get; set; }
        public string Content { get; set; }

    }
}
