﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class LoanTransaction : BaseEntity
    {
        public int LoanTransactionId { get; set; }
        public Guid LoanId { get; set; }
        public string TransactionId { get; set; }
        public string TransactionStatus { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Message { get; set; }

        public virtual LoanTransaction Loan { get; set; }
    }
}
